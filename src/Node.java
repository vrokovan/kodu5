// $Id: Node.java 370 2013-03-21 17:51:37Z ylari $



/**
 * Write a method to build a tree from the left parenthetic string representation (return the root node of the
 * tree) and a method to construct the right parenthetic string representation of a tree represented by the root
 * node this (see http://enos.itcollege.ee/~jpoial/algoritmid/Puupraktikum.html).
 * @author <a href="mailto:ulari.ainjarv@itcollege.ee?subject=I231: Node.java ...">�lari Ainj�rv</a>
 * @version $Revision: 370 $
 */
public class Node {

	/** Node name. */
	private String name;

	/** First child. */
	private Node firstChild;

	/** Next sibling. */
	private Node nextSibling;

	/** Default constructor. */
	public Node() {
	}

	/**
	 * Initializes a new node from a given name.
	 * @param n a name of the node.
	 */
	public Node(String n) {
		name = n;
	}

	/**
	 * Initializes a new node.
	 * @param n a name of the node.
	 * @param d first child node.
	 * @param r next sibling node.
	 */
	public Node(String n, Node d, Node r) {
		name = n;
		firstChild = d;
		nextSibling = r;
	}

	/**
	 * Method to build a tree from the left parenthetic string representation.
	 * @param s a string representation of the tree.
	 * @return a root node of the tree.
	 */
	public static Node parseTree(String s) {
		if (s != null && !s.isEmpty()) {
			Node root = new Node();
			StringBuilder b = new StringBuilder();
			for (int i = 0; i < s.length(); i++) {
				if (s.charAt(i) == '(') { // expecting children
					root.name = b.toString();
					parse(s.substring(i + 1, s.length() - 1), root, true);
					b.setLength(0);
					break;
				} else // increase name
					b.append(s.charAt(i));
			}
			if (b.length() > 0 && root.firstChild == null)
				root.name = b.toString();
			return root;
		}
		else
			throw new RuntimeException("Can't parse empty string.");
	}

	/**
	 * Helper to parse a tree from string.
	 * @param s a string representation of subtree.
	 * @param node an active node we are processing.
	 * @param root indicates weather this node is parent node.
	 * @return cursor position.
	 */
	private static int parse(String s, Node node, boolean root) {
		StringBuilder b = new StringBuilder();
		int i = 0;
		while (i < s.length()) {
			switch (s.charAt(i)) {
				case '(':
					if (b.length() > 0) {
						node = create(node, b.toString(), root);
						i += (root) ? parse(s.substring(i + 1), node.firstChild, true) : parse(s.substring(i + 1), node.nextSibling, true);
						if (root)
							node = node.firstChild;
						else
							node = node.nextSibling;
						b.setLength(0);
					}
					break;
				case ',':
					if (b.length() > 0) {
						node = create(node, b.toString(), root);
						i += (root) ? parse(s.substring(i + 1), node.firstChild) : parse(s.substring(i + 1), node.nextSibling);
					} else
						i += parse(s.substring(i + 1), node);
					return ++i;
				case ')':
					if (b.length() > 0)
						node = create(node, b.toString(), root);
					return ++i;
				default:
					b.append(s.charAt(i));
					break;
			}
			i++;
		}
		if (b.length() > 0)
			node = create(node, b.toString(), root);
		return i;
	}

	/**
	 * Helper to parse a tree from string (in case if not parent node).
	 * @param s a string representation of subtree.
	 * @param node an active node we are processing.
	 * @return cursor position.
	 */
	private static int parse(String s, Node node) {
		return parse(s, node, false);
	}

	/**
	 * Helper to create appropriate child for the given node.
	 * @param node an active node we are processing.
	 * @param name a name for the child.
	 * @param root indicates weather this node is parent node.
	 * @return the node we are processing.
	 */
	private static Node create(Node node, String name, boolean root) {
		if (root)
			node.firstChild = new Node(name);
		else
			node.nextSibling = new Node(name);
		return node;
	}

	/**
	 * Method to construct the right parenthetic string representation of a tree.
	 * @return string representation of the tree.
	 */
	public String rightParentheticRepresentation() {
		StringBuilder ret = new StringBuilder();
		Node node = firstChild;
		if (firstChild != null)
			ret.append('(');
		while (node != null) {
			ret.append(node.rightParentheticRepresentation());
			node = node.nextSibling;
			if (node != null)
				ret.append(',');
		}
		if (firstChild != null)
			ret.append(')');
		return ret.append(name).toString();
	}

	/**
	 * Main entry point.
	 * @param args command line arguments.
	 */
	public static void main(String[] args) {
		String s = "A(B,C)";
		// String s = "+(*(-(2,1),4),/(6,3))"; // debug complex tree (expecting "(((2,1)-,4)*,(6,3)/)+")
		Node t = Node.parseTree(s);
		String v = t.rightParentheticRepresentation();
		System.out.println(s + " ==> " + v); // A(B,C) ==> (B,C)A
	}

}
